package gov.cdc.nssp.essence

data class OregonPayload(
    val element: List<OregonPayloadRecord>
)
data class OregonPayloadRecord(
    var caseid: String,
    var Date: String,
    var ddsex: String,
    var Age: String,
    var dplacetypecode: String,
    var dresstatefips: String,
    var drescountyfips: String,
    var drescountryal: String,
    var ddreszip: String,
    var CONSQ1: String,
    var CONSQ2: String,
    var CONSQ3: String,
    var CONSQ4: String,
    var OTH_SIGNF_CONDS: String,
    var ICD_CODE: String,
    var ICD_Description: String,
    var ACME1: String,
    var ACME2: String,
    var ACME3: String,
    var ACME4: String,
    var ACME5: String,
    var ACME6: String,
    var ACME7: String,
    var ACME8: String,
    var ACME9: String,
    var ACME10: String,
    var ACME11: String,
    var ACME12: String,
    var ACME13: String,
    var ACME14: String,
    var ACME15: String,
    var ACME16: String,
    var ACME17: String,
    var ACME18: String,
    var ACME19: String,
    var ACME20: String,
    var ProcessDate: String,
    var InitialLiteralCauseOfDeath: String,
    var ACMECodes: String,
    var ACME_OR_Literal: String,
    var dregstatdate: String,
    var Region: String,
    var ZipCode: String,
    var FemPregCode: String,
    var InjuryHowOccur: String,
    var InjuryPlace: String,
    var ddateofevent: String,
    var dplacefacilname: String,
    var dplacestnum: String,
    var dplacepredir: String,
    var dplacestname: String,
    var dplacestdesig: String,
    var dplacepostdir: String,
    var dplaceunitnum: String,
    var dplacecitylit: String,
    var dplacecountylit: String,
    var dplacestatelit: String,
    var dplacecountrylit: String,
    var dplacefaciltype: String,
    var dplacefaciltypeother: String,
    var DateTimeAdded: String,
    var WeekYear: String,
    var MonthYear: String,
    var QuarterYear: String,
    var Year: String,
    var AgeGroup: String
) {

    companion object {
        private fun convertToSteveStandard(oregonPayloadRecord: OregonPayloadRecord): MortalityRecord {
            return MortalityRecord(
                FILENO = oregonPayloadRecord.caseid,
                SEX = oregonPayloadRecord.ddsex,
                AGE = oregonPayloadRecord.Age
                )
        }
    }
}

