package gov.cdc.nssp.essence

import com.google.gson.Gson
import kong.unirest.Unirest


class OregonAPI {
    private val url: String = "https://essence.oha.oregon.gov/oregon_state/api/dataDetails/?endDate=07Jul2020&timeResolution=daily&percentParam=noPercent&userId=393&geographySystem=region&datasource=va_ordeathrecords&aqtTarget=DataDetails&detector=nodetectordetector&startDate=01Jul2020"
    private val gson = Gson()

    fun getJsonPayload(): OregonPayload {
        Unirest.config()
                .verifySsl(false)
                .setDefaultBasicAuth("NSSP-Mortality", "3$&X#e+2")


        val result = Unirest.get(url)
                .asJson()
                .body
                .array
                .getJSONObject(0)
                .getJSONArray("dataDetails")

        val resultJson = this.gson.toJson(result)
        return this.gson.fromJson(resultJson, OregonPayload::class.java)
    }
}